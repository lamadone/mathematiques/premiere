---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Suites

## Définitions

### Généralités sur les suites

```{proof:definition} suite
Une suite est une partie d'un ensemble avec d'éventuels répétitions indexés
par $\\N$.
```

Autrement dit, on numérote des éléments pris dans un ensemble $E$, souvent
$\\R$

```{proof:example}
+ $u$ définie par $\forall n \in \\N,\ u_n = 1$
+ $u$ définie par $\forall n \in \\N,\ u_n = (-1)^n$
```

```{proof:definition} égalité de suites
Soient $u$ et $v$ deux suites. On dit que $u = v$ si et seulement si
$\forall n \in \\N,\ u_n = v_n$.
```

```{proof:definition} variations des suites
On dit qu'une suite est

constante
:   $\forall n \in \\N, u_n = k$

stationnaire
:   Il existe $N \in \\N$ tel que $\forall n ≥ N,\ u_n = k$
    Autrement dit, il existe un rang $N$ à partir duquel la suite est
    constante.

croissante (resp. décroissante)
:   $\forall n \in \\N,\ u_{n+1} ≥ u_n$ (resp. $u_{n+1} ≤ u_n$)
```

### Mode de génération d'une suite suite

```{proof:definition} avec une «formule» explicite
On dispose d'une expression de $u_n$ qui fait intervenir $n$.
```

```{proof:definition} avec une relation de récurrence
On dispose de $u_0$ le premier terme et d'une expression qui donne $u_{n+1}$
en fonction de $u_n$.
```

```{proof:example} factorielle d'un nombre
$\left\{\begin{array}{l}u_0 = 1 \\ u_{n+1} = (n+1)×u_n \end{array}\right.$
```

### Quelques suites particulières

```{proof:definition} suite arithmétique
Une suite définie par la relation de récurrence $u_{n+1}  = u_n + r est dite
arithmétique.
```
```{proof:proposition}
Une suite arithmétique est telle que, pour tout $n$ entier naturel,
    $u_{n+1} - u_n = r$.
```
```{proof:proposition}
Une suite arithmétique s'écrit aussi $u_n = u_0 + n×r$
```
```{proof:definition} suite géométrique
Une suite définie par la relation de récurrence $u_{n+1}  = u_n × q est dite
géométrique.
```
```{proof:proposition}
Une suite géométrique est telle que, pour tout $n$ entier naturel, $\frac{u_{n+1}}{u_n} = q$.
```
```{proof:proposition}
Une suite géométrique s'écrit aussi $u_n = u_0 × q^n$
```
