---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3.9.8 64-bit
  language: python
  name: python3
---

# Algorithmes sur les suites

## Autre façon de définir une suite

On considère la suite des inverses, définies sous la forme d'une fonction de
$\mathbb{N} \to \mathbb{N}$ par $u: n \mapsto \frac1n$.

```{code-cell} ipython3
def u(n):
  return 1/n
```

On cherche pour quelle valeur de $n$, $u_n ≤ \varepsilon$, où $\varepsilon$
est une quantité fixée.

```{code-cell} ipython3
eps = 10**(-6)
n = 1
while u(n) > eps:
  n = n + 1
n
```
