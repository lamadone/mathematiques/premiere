---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.9.8 64-bit
  language: python
  name: python3
---

# Utilisation des suites

## Définir une suite avec Python

Pour utiliser des suites avec Python, on va essentiellement utiliser les `list` qui sont des tableaux dynamiques. Les programmes suivant ont pour but de vous permettre de manipuler les suites aisément et de concevoir par la suite vos propres fonctions pour générer les premiers termes d'une suite numérique afin d'en estimer le comportement.

### Utilisation de `range`

+++



```{code-cell} ipython3
u = [n for n in range(10)]
u
```

L'expression `n in range(10)` a permis de …

```{code-cell} ipython3
u = [n**2 for n in range(10)]
u
```

On peut effectuer des … dans …

```{code-cell} ipython3
u[0]
```

`u[0]` correspond au … terme de la suite $(u_n)_{n\in\mathbf{N}}$

```{code-cell} ipython3
u[-1]
```

`u[-1]` correspond au … terme généré de la suite suite $(u)_{n\in\mathbf{N}}$.

+++

### Exercices

1. Écrire la suite des 10 premiers termes de la suite alternée de terme général $u_n = (-1)^n$.
2. Écrire la suite des 20 premiers termes de la suite de terme général $v_n = 2×n -5$
3. Écrire la suite des 10 premiers termes de la suite de terme général $w_n = 2^n + 1$
4. Écrire la suite des 50 premiers termes de la suite de terme général $y_n = \sqrt{1 - (\frac{n - 25}{50})^2}$.
    
   On pensera à ajouter `import math` au début et on utilisera `math.sqrt()` pour $\sqrt{⋅}$
5. Peut-on écrire la suite des 10 premiers inverses ?

+++

### Utilisation avancée de `range`

```{code-cell} ipython3
inverses = [1/n for n in range(1,10)]
inverses
```

```{code-cell} ipython3
len(inverses)
```

La fonction `range(1, 10)` permet de renvoyer les éléments $n$ compris entre les entiers $1 … n … 10$.

+++

La fonction `range(1, 10)` a renvoyé … entiers, alors que la fonction `range(10)` renvoyait … entiers.

+++

La fonction `len(inverses)` renvoie …

+++



+++



+++



+++
