---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercices

:::{exercise}
On considère la suite $u$ définie par $\forall n \in \\N$ par

$$ \left\{\begin{array}{l}u_0 = 1 \\ u_{n+1} = \frac9{6 - u_n} \end{array} \right. $$

et la suite $w$ définie par $v_n = \frac1{u_n - 3}$
1. Démontrer que $v$ est arithmétique de raison $-\frac13$
2. En déduire $v_n$ en fonction de $n$, puis $u_n$ en fonction de $n$.
:::


:::{exercise}
Soit $(u_n)$ la suite définie par $u_0 = \frac12$ et $\forall n \in \\N$,

$$ u_{n+1} = \frac{u_n}{1 + 2u_n} $$

1. Compléter l'algorithme suivant pour qu'il affiche $u_n$, $n$ étant choisi par l'utilisateur :

    ```python
    def calcul_suite(rang):
        u_n = ...
        for indice in ...:
            u_n = ...
        return u_n
    ```
    Soit $(v_n)$ la suite définie $\forall n \in \\N$ par $v_n = \frac1{u_n} + 1$

2. Déterminer $u_1$ et $u_2$, puis $v_0$, $v_1$ et $v_2$.
3. Que peut-on conjecturer quand à la nature de la suite $v$ ?
    Démontrer la conjecture émise.
4. En déduire $v_n$ puis $u_n$ en fonction de $n$.
5. Étudier la monotonie de la suite $u$.
6. Montrer que la suite $u$ est bornée[^bornée]
7. À l'aide de la calculatrice, conjecturer la convergence de la suite $u$.
8. Que permet de mettre en évidence l'algorithme suivant ?
    ```python
    def mystere():
        eps = 10**(-6)
        n  = 0
        while 1/(2 + 2*n) > eps:
            n = n + 1
        return n
    ```
:::

::::{exercise}
 Dans une entreprise, on s'intéresse à la probabilité qu'un salarié soit absent durant une période d'épidémie de grippe.
+ Un salarié malade est absent
+ La première semaine de travail, le salarié n'est pas malade.
+ Si la semaine $n$ le salarié n'est pas malade, il tombe malade la semaine $n+1$ avec une probabilité égale à 0,04.
+ Si la semaine $n$ le salarié est malade, il reste malade la semaine $n+1$ avec une probabilité égale à 0,24.

On désigne, pour tout entier naturel nnn supérieur ou égal à 1, par $E_{n}$ l'évènement «le salarié est absent pour cause de maladie la $n$-ième semaine». On note $p_{n}$ la probabilité de l'évènement $E_{n}$.

On a ainsi : $p_{1}=0$ et, pour tout entier naturel $n$ supérieur ou égal à 1 : $0\leqslant p_{n}<1$.

1. 
    1. Déterminer la valeur de $p_3$ à l'aide de l'arbre de probabilités
    2. Sachant que le salarié a été absent pour cause de maladie la troisième semaine, déterminer la probabilité qu'il ait été aussi absent pour cause de maladie la deuxième semaine. 

2. Recopier sur la copie et compléter l'arbre de probabilité donné ci-dessous.

    :::{mermaid}
    %%{ init : { "flowchart" : { "curve" : "cardinal" }}}%%
    %%
    flowchart LR
        racine -- "pₙ" ---A(("Eₙ"))
        racine(( )) -- "1-pₙ" ---B(("̅E̅ₙ̅ "))
        A -- "…" ---VA(("Eₙ₊₁"))
        A -- "…" ---FA(("̅E̅ₙ̅₊̅₁"))
        B -- "…" ---VB(("Eₙ₊₁"))
        B -- "…" ---FB(("̅E̅ₙ̅₊̅₁"))
    :::

    On admet que $p_{n+1} = 0,2p_n + 0,04$.

3. Démontrer que la suite $u$ définie par $u_n = p_n - 0,05$ pour $n ≥ 1$ est géométrique. Donner la raison.
En déduire l'expression de $u_n$ en fonction de $n$.

::::

[^bornée]: Une suite est dite bornée quand elle est majorée et minorée, c'est à dire que tous les termes de la suite $u$ appartiennent à un intervalle borné $[m ; M]$.
