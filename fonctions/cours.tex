\documentclass[../cours_premiere_generale_maths.tex]{subfiles}

\title{Fonctions de référence}
\author{Vincent-Xavier Jumel}
\date{}

\begin{document}


\ifSubfilesClassLoaded{%
  \maketitle%
}{}

\section{Fonction numérique}

\subsection{Ensemble de définition}

\begin{definition}
  Une fonction numérique $f$ d'une variable réelle $x$ est une relation qui
  a un nombre réel  $x$ associe un unique réel $f(x)$. On note $f :
  \mapsto f(x)$.
\end{definition}

\begin{definition}
L'ensemble de définition d'une fonction $f$ est l'ensemble des valeurs de la variable $x$ pour lesquelles la fonction est définie.\\
\end{definition}

\begin{proposition}
On dit que deux fonctions $f$ et $g$ sont égales si et seulement si :
\begin{itemize}[label=\textbullet]
\item Elles ont le même ensemble de définition : $D_f = D_g$
\item Pour tout $x\in D_f$, $f(x) = g(x)$.\\
\end{itemize}
\end{proposition}

\subsection{Vocabulaire}

Soient $f$ et $g$ deux fonction définies sur un intervalle $I$.
\begin{itemize}
  \item $f$ est inférieure (respectivement strictement inférieure) à $g$, si
    et seulement si, pour tout $x \in I,\ f(x) ≤ g(x)$ (resp. $f(x) < g(x)$
  \item $f$ est supérieure (respectivement strictement supérieure) à $g$, si
    et seulement si, pour tout $x \in I,\ f(x) ≥ g(x)$ (resp. $f(x) > g(x)$
  \item $f$ est négative (respectivement strictement négative), si
    et seulement si, pour tout $x \in I,\ f(x) ≤ 0$ (resp. $f(x) < 0$
  \item $f$ est positive (respectivement strictement positive) à $g$, si
    et seulement si, pour tout $x \in I,\ f(x) ≥ 0$ (resp. $f(x) > 0$
  \item $f$ admet un minimum $x_m$ si et seulement si $\forall x \in I,\
    f(x) ≥ m$ et $f(x_m) = m$.
  \item $f$ admet un maximum $x_M$ si et seulement si $\forall x \in I,\
    f(x) ≤ M$ et $f(x_M) = M$.
  \item $f$ admet un extremum si et seulement si $f$ admet un minimum ou un
    maximum.
\end{itemize}

\begin{information}[Majorant, minorant, borne supérieure, borne inférieure]
  Une fonction $f$ est majorée par $M$ (respectivement minorée par $m$) si
  $\forall x \in I$, $f(x) ≤ M$ (resp. $f(x) ≥ m)$. On dit alors que $M$ est
  \textbf{un} majorant (resp. minorant) et on note que celui-ci n'est pas
  unique.

  Le plus petit des majorants est la borne supérieure et le plus grand des
  minorants est la borne inférieure. Attention, la borne supérieure n'est
  pas nécessairement atteinte. On peut considérer la fonction $f$ définie
  par $f(x) = \frac{1}{x} + 4$ sur $\interoo{0 +\infty}$. Cette fonction
  admet 4 comme borne inférieure, mais celle-ci n'est pas atteinte.
\end{information}

\subsection{Variation d'une fonction}

Soit un intervalle $I$, $a$ et $b$ deux réels de $I$. Soit une fonction $f$ définie au moins sur $I$.
\begin{itemize}[label=\textbullet]
  \item $f$ est croissante (respectivement strictement croissante) sur $I$
    si et seulement si $\forall (x,y) \in I^2,\ x < y \implies f(x) ≤ f(y)$
    (resp. $(f(x) < f(y)$).
  \item $f$ est décroissante (respectivement strictement décroissante) sur
    $I$ si et seulement si $\forall (x,y) \in I^2,\ x < y \implies f(x) ≤
    f(y)$ (resp. $(f(x) < f(y)$).
  \item $f$ est monotone si et seulement si $f$ est croissante ou
    décroissante.
\end{itemize}

\begin{note}
  Les variations d'une fonction peuvent changer sur l'intervalle de
  définition. Il faut souvent le découper en plus petits intervalles.
\end{note}

La courbe représentative d'une fonction $f$ dans un repère est l'ensemble
des points $M(x ; f(x))$ avec $x$ un élément de l'ensemble de définition de
la fonction. $y = f(x)$ est une équation de la courbe représentative.

\section{Fonctions de référence}
\subsection{Fonction affine}
Une fonction affine $f$ est une fonction définie sur $\R$ par $f(x) = m x +
p$.

La représentation graphique d'une fonction affine est une droite qui passe
par le point de coordonnées $(0 ; p)$.

$m$ est le coefficient directeur de la droite.

Le point de coordonnées $(0 ; p)$ est l'ordonnée à l'origine.

\subsubsection{Variation et signe de la fonction}

\begin{multicols}{2}
  Si $m < 0$ alors la fonction $f$ est décroissante

  \begin{tikzpicture}
    \tkzTabInit[espcl=2.5]
    {$x$ / 1   , $f(x)$ / 1}%
    {$\-\infty$, $ $,$+\infty$  }%
    \tkzTabLine{,+,0,-,}
  \end{tikzpicture}

  \begin{tikzpicture}
    \tkzTabInit[espcl=4.5]
    {$x$ / 1  ,$f$/2}%
    {$-\infty$ , $+\infty$ }%
    \tkzTabVar{+/$+\infty$,-/$-\infty$}
  \end{tikzpicture}

  Si $m > 0$ alors la fonction $f$ est croissante

  \begin{tikzpicture}
    \tkzTabInit[espcl=2.5]
    {$x$ / 1   , $f(x)$ / 1}%
    {$\-\infty$, $ $,$+\infty$  }%
    \tkzTabLine{,-,0,+,}
  \end{tikzpicture}

  \begin{tikzpicture}
    \tkzTabInit[espcl=4.5]
    {$x$ / 1  ,$f$/2}%
    {$-\infty$ , $+\infty$ }%
    \tkzTabVar{-/$-\infty$,+/$\infty$}
  \end{tikzpicture}
\end{multicols}
\textcolor{teal}{\subsection{Fonction du second degré}}
On appelle fonction polynôme du second degré toute fonction définie sur $\R$ par
\begin{center}
  \dotfill \\
\end{center}
La représentation graphique d'une fonction du second degré est une.......................................... d'axe verticale et de sommet $S\left( -\dfrac{b}{a} ; f\left(  -\dfrac{b}{a}\right) \right) $ dirigée vers le haut si $a > 0$ et dirigée vers le bas si $a < 0$.\\

 Le signe d'une fonction du second degré dépend de la position de la parabole.\\
  (Discriminant, racines)\\
 
 \textcolor{violet}{Exercices 1, 2 , 3, 4 page 80 }

  \textcolor{teal}{\subsection{Fonction homographique}}
  On appelle fonction homographique, une fonction $f$ définie sur $\R -\lbrace\alpha\rbrace$ et qui peut se mettre sous la forme  $f(x) = \dfrac{a}{x-\alpha} + \beta$.\\
  La représentation d'une fonction homographique est une ...................................centrée en $S(\alpha ; \beta)$.\\
  Variation : 
  \begin{multicols}{2}
  Si $a > 0$\\
 \begin{tikzpicture}
      \tkzTabInit[espcl=2.5]
      {$x$ / 1  ,$f$/2}%
      {$-\infty$ , $ $ ,$+\infty$ }%
      \tkzTabVar{}
    \end{tikzpicture}

  Si $a < 0$\\
 \begin{tikzpicture}
      \tkzTabInit[espcl=2.5]
      {$x$ / 1  ,$f$/2}%
      {$-\infty$ , $ $ ,$+\infty$ }%
      \tkzTabVar{}
    \end{tikzpicture}
\end{multicols}

 \textcolor{violet}{Exercices 5, 8, 12 page 80 }
 \textcolor{teal}{\subsection{Fonction racine carrée}}
 
 La fonction racine carrée est la fonction $f$ définie sur $\R_+$ par $f(x) = $\dotfill.\\
 La fonction racine carrée est ................................... sur $\R_+$.\\
 La fonction racine carrée est................................... sur $\R_+$.\\

Démonstration :\\
Soit deux réels $a$ et $b$ tel que $a > b$. \'Etudiez le signe de $\sqrt{a}-\sqrt{b}$ : \\
\ligne[8]
 
 \textbf{\textcolor{cyan}{Propriétés :}}\\
\begin{itemize}[label=\textbullet]
  \item Si $a \geqslant 0$ alors $\sqrt{a^2} = a$.
  \item Si $a \leqslant 0$ alors $\sqrt{a^2} = -a$.
  \item Si $a \geqslant 0$ et $b \geqslant 0$ alors $\sqrt{ab} = \sqrt{a} \times \sqrt{b}$.
    \item Si $a \geqslant 0$ et $b >0$ alors $\sqrt{\dfrac{a}{b}} = \dfrac{\sqrt{a}}{\sqrt{b}}$.
  \end{itemize}

\begin{multicols}{2}
\begin{tikzpicture}
      \tkzInit[xmin=0,xmax=7,ymax=4]
      \tkzGrid
      \tkzAxeXY
    \draw [domain=0:7][draw=red, very thick] plot  (\x, {sqrt(\x)});
 \end{tikzpicture}
  \begin{tikzpicture}
      \tkzTabInit[espcl=2.5]
      {$x$ / 1  ,$f$/2}%
      {$ $ , $ $ ,$ $ }%
      \tkzTabVar{}
    \end{tikzpicture}
\end{multicols}
 
 Exemple : Résoudre l'équation  $\sqrt{x-2}=10$\\
 Déterminer l'ensemble de définition : \\
\ligne[2]
 \'Elever les deux membres de l'égalité au carré :\\
\ligne[1]
  Résoudre l'équation obtenue : \\
  \ligne[3]
 
  \textcolor{violet}{Exercices 16, 17 page 81 }
 \textcolor{teal}{\subsection{Fonction valeur absolue}}
 On appelle valeur absolue d'un nombre réel $x$, le nombre noté $\varabs{x}$ tel que : 
 \begin{center}
$\varabs{x} = x $  si $ x $ est positif.\\
$\varabs{x} = - x $ si $ x $ est négatif.\\
 \end{center}
 
  \textbf{\textcolor{cyan}{Propriétés :}}\\
\begin{itemize}[label=\textbullet]
  \item   $\forall x \in \R$ on a $\varabs{x} \geqslant 0$.
  \item $\forall x \in \R$ on a $\varabs{x} = 0$ équivaut à $x = 0$
  \item $\forall x \in \R$ on a $\varabs{-x} = \varabs{x}$.
  \item $\forall x \in \R$ on a $\varabs{x^2} = x^2$.
  \item $\forall x \in \R$ on a $\varabs{x}^2 = x^2$
  \item $\forall x\in \R$ et $\forall y\in \R$ on a $\varabs{xy} = \varabs{x} \times \varabs{y}$.
  \item $\forall x\in \R$ et $\forall y\in \R$ on a $\varabs{x + y} \leqslant \varabs{x} + \varabs{y}$. Inégalité triangulaire
  \item $\forall x\in \R$et $\forall y\in \R$ on a $\varabs{x} = \varabs{y}$ équivaut à $x = y$ ou $x = -y$.
  \item $\forall x \in \R$ on a $\sqrt{x^2} = \varabs{x}$.
      
  \end{itemize}

Exemple : Résoudre dans $\R$ l'équation suivante : $\varabs{2x - 2} = \varabs{3 - x}$\\
\ligne[5]

 \textbf{\textcolor{cyan}{Variation :}}\\
La fonction valeur absolue est une fonction affine définie par morceaux.\\

Sa représentation graphique est composées de \dotfill.\\

La fonction valeur absolue est strictement ...................................... sur l'intervalle ] $-\infty$ ; 0].\\

La fonction valeur absolue est strictement  ......................................  sur l'intervalle [0 ; $+\infty$ [.\\

La courbe représentative a pour axe de symétrie l'axe des ordonnées.
\begin{multicols}{2}
\begin{tikzpicture}
      \tkzInit[xmin=-3.8,xmax=3.8,ymin=-2,ymax=4]
      \tkzGrid
      \tkzAxeXY
    \draw [domain=0:3.8] [draw=red, very thick] plot  (\x, \x);
     \draw [domain=-3.8:0] [draw=red, very thick] plot  (\x, {-\x});
 \end{tikzpicture}
 
 \begin{tikzpicture}
      \tkzTabInit[espcl=2.5]
      {$x$ / 1  ,$f$/3}%
      {$-\infty$,$ $ , $+\infty$ }%
      \tkzTabVar{}
    \end{tikzpicture}
\end{multicols}
La fonction valeur absolue $\varabs{x - a}$ représente la distance de $x$ au nombre $a$.\\
Soient $a$ un nombre réel et $r$ un nombre réel strictement positif. \\
On a : $\varabs{x - a} \leqslant r$ si et seulement si $x \in [a - r ; a + r]$.\\
Exemple : Résoudre $\varabs{x - 2} < 1$\\
\ligne[5]

 \textcolor{violet}{Exercices 20, 22, 23, 24, 25, 27 page 81 - 82 }

\textcolor{blue}{\section{Fonctions associées}}
 \textcolor{teal}{\subsection{Fonction $u + k$}}
 Soit une fonction $u$ définie sur un ensemble $D$ et $k$ un nombre réel.\\
 La fonction $u + k$ est définie sur l'ensemble $D$ par $(u + k)(x) = u(x) + k$.\\
 
 Les fonctions $u$ et $u + k$ ont \dotfill.\\
 La représentation graphique de la fonction $u + k$ se déduit de la représentation graphique de la fonction $u$ en faisant une translation de vecteur $\vecteur{v}\left(\begin{array}{c}0\\ k\end{array}\right) $.\\
 
  \textcolor{teal}{\subsection{Fonction $\lambda u$}}
  Soit une fonction $u$ définie sur un ensemble $D$ et $\lambda$ un nombre réel.\\
  La fonction $\lambda u$ est définie sur l'ensemble $D$ par $(\lambda u)(x) = \lambda \times u(x)$.\\
\begin{itemize}[label=\textbullet]
  \item Si $\lambda > 0$, les fonctions $u$ et $\lambda u$ ont \dotfill\\
  
  \item Si $\lambda < 0$, les fonctions $u$ et $\lambda u$ ont \dotfill\\
  
  \item Si $\lambda = 0$, la fonction $\lambda u$ est la fonction constante nulle.\\
  
  \end{itemize}
  
   \textcolor{violet}{Exercices 43, 45, 46  page 83 }
\textcolor{teal}{\subsection{Fonction $\sqrt{u}$}}
    Soit une fonction $u$ définie sur un ensemble $D$ telle que pour tout $x\in D$, $u(x) \geqslant 0$.\\
     La fonction $\sqrt{u}$ est définie sur l'ensemble $D$ par $\sqrt{u(x)}$.\\

    Les fonctions $u$ et $\sqrt{u}$ ont \dotfill 
     
Démonstration : 
\begin{itemize}
\item   Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante donc \dotfill \\
  
  La fonction racine carrée est croissante sur l'intervalle $[0; +\infty[ $ donc \dotfill \\
  
  On a donc que la fonction $\sqrt{u}$ est .....................................\\
  
      Les fonctions $u$ et $\sqrt{u}$ ont \dotfill \\
      \item Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est décroissante donc  \dotfill \\
  
  La fonction racine carrée est croissante sur l'intervalle  $[0; +\infty[ $ donc  \dotfill \\
  
  On a donc que la fonction $\sqrt{u}$ est .....................................\\
\end{itemize}
       Les fonctions $u$ et $\sqrt{u}$ ont \dotfill 
\textcolor{teal}{\subsection{Fonction $\dfrac{1}{u}$}}
    Soit une fonction $u$ définie sur un ensemble $D$ telle que pour tout $x\in D$, $u(x) \neq 0$.\\
    La fonction $\dfrac{1}{u}$ est définie sur l'ensemble $D$ par $\dfrac{1}{u}(x) = \dfrac{1}{u(x)}$.\\
\begin{itemize}
   \item Soit $u$ une fonction monotone et strictement positive sur un intervalle $I$, les fonctions $u$ et $\dfrac{1}{u}$ ont \dotfill
   \item Soit $u$ une fonction monotone et strictement négative sur un intervalle $I$, les fonctions $u$ et $\dfrac{1}{u}$ ont \dotfill
   
\end{itemize}    
Démonstration : 
\begin{itemize}
\item   Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante et strictement positive donc \dotfill \\
  
  La fonction inverse est décroissante  sur l'intervalle $]0; +\infty[$,  alors \dotfill \\
  
  La fonction $\dfrac{1}{u}$ est ....................................\\
  Soit $u$ une fonction strictement positive, les fonctions $u$ et $\dfrac{1}{u}$ \dotfill
  \item   Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante et strictement négative donc \dotfill \\
  
  La fonction inverse est décroissante  sur l'intervalle $]-\infty ; 0[$,  alors \dotfill \\
  
  La fonction $\dfrac{1}{u}$ est est ....................................\\
  Soit $u$ une fonction strictement positive, les fonctions $u$ et $\dfrac{1}{u}$ \dotfill
\end{itemize}

   \textcolor{violet}{Exercices 52, 53 page 84 }
\end{document}



