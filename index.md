# Présentation de ce cours

Ces notes illustrent certains aspects du cours de mathématiques de première générale.

```{tableofcontents}
```

## Liste des fichiers créés

On trouvera ici la liste des fichiers distribués en cours, éventuellement
actualisés dans leur dernière version.


