\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Statistiques}
\author{1\up{ère}S}
\date{}

\begin{document}


\maketitle\\

\section{Généralités}
Une étude statistique descriptive s'effectue sur une \textcolor{red}{population} (des personnes, des villes, des objets...) dont les éléments sont des \textcolor{red}{individus} et consiste à observer et étudier un même aspect sur chaque individu, nommé \textcolor{red}{caractère} (taille, nombre d'habitants, consommation...).

Il existe deux types de caractères :
\begin{enumerate}
\item  \textit{quantitatif} : c'est un caractère auquel on peut associer un nombre c'est-à-dire, pour simplifier, que l'on peut mesurer. On distingue alors deux types de caractères quantitatifs :
 \begin{itemize}
 \item  \textit{discret} : c'est un caractère quantitatif qui ne prend qu'un nombre fini de valeurs. Par exemple le nombre d'enfants d'un couple.
 \item  \textit{continu} : c'est un caractère quantitatif qui, théoriquement, peut prendre toutes les valeurs d'un intervalle de l'ensemble des nombres réels. Ses valeurs sont alors regroupées en classes. Par exemple la taille d'un individu, le nombre d'heures passées devant la télévision.
 \end{itemize}
\item  \textit{qualitatif} : comme la profession, la couleur des yeux, la nationalité. Dans ce dernier cas, \og nationalité française \fg, \og nationalité allemande \fg{} etc. sont les modalités du caractère.
\end{enumerate}
En général une série statistique à caractère discret se présente sous la forme :
	\begin{center}
	\begin{tabular}{|c|c|c|p{2cm}|c|}
			\hline
			Valeurs & $x_{1}$ & $x_{2}$ & $\ldots\ldots\ldots\ldots$ & $x_{p}$  \\
			\hline
			Effectifs & $n_{1}$ & $n_{2}$ & $\ldots\ldots\ldots\ldots$ & $n_{p}$  \\
			\hline
			Fréquences & $f_{1}$ & $f_{2}$ & $\ldots\ldots\ldots\ldots$ & $f_{p}$  \\
			\hline
	\end{tabular}
	\end{center}
	
	Le terme $x_i$ de la série est la valeur prise par le caractère $x$ pour l'individu numéro $i$ de la population.\\
	L'effectif $n_i$ représente le nombre d'individus de la population dont le caractère vaut $x_i$.\\
	La fréquence $f_i$ est le quotient  $\dfrac{n_i}{N}$.\\
On écrira souvent : la série $(x_{i},n_{i})$. (On n'indique pas le nombre de valeurs lorsqu'il n'y a pas d'ambiguïté). Souvent on notera $N$ l'effectif total de cette série donc $N=n_{1}+n_{2}+ \cdots +n_{p}$.

Lorsqu'une série comporte un grand nombre de valeurs, on cherche à la résumer, si possible, à l'aide de quelques nombres significatifs appelés paramètres.

effectif total
fréquence

\textit{Série 1}

Une étude sur le nombre d'employés dans les commerces du centre d'une petite ville a donné les résultats suivants :

\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\hline
Nombre d'employés & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\ \hline
Effectif & 11 & 18 & 20 & 24 & 16 & 14 & 11 & 6 \\ \hline
\end{tabular}
\end{center}

\textit{Série 2}

Une étude sur la durée de vie en heures de 200 ampoules électriques a donné les résultats suivants :

\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline 
Durée de vie en centaine d'heures & [12 ; 13[ & [13 ; 14[ & [14 ; 15[ & [15 ; 16[ & [16 ; 17[ \\ \hline
Effectif & 28 & 46 & 65 & 32 & 29 \\ \hline
\end{tabular}
\end{center}

\textit{Série 3}

Une étude sur la couleur préférée de 60 personnes a donné les résultats suivants: 

\begin{tabular}{|l|c|c|c|c|}
\hline 
Couleur & Rouge & Bleu & Vert & Jaune \\ 
\hline 
Effectif & 12 & 28 & 7 & 13 \\ 
\hline 
\end{tabular} 

\section{Paramètres de position}
\subsection{Paramètres de position de tendance centrale}
\subsubsection*{Mode - Classe modale}
\begin{definition}
Le mode d'une série statistique est la valeur du caractère qui correspond au plus grand effectif.

Dans le cas d'une série à caractère quantitatif continu dont les valeurs sont regroupées en classes, la classe modale est la classe de plus grand effectif.
\end{definition}

Remarque : Il peut y avoir plusieurs modes ou classes modales.

\begin{exemple}
Déterminer le mode et la classe modale des séries 1, 2 et 3.
\end{exemple}
\rep{Pour la série 1, le mode est 4.\\
Pour la série 2, la classe modale est l'intervalle $[14;15[$.\\
Pour la série 3, le mode est Bleu.
}

\subsubsection*{Médiane}
\begin{definition}
La médiane d'une série statistique est un réel noté $M_e$ tel que au moins 50\% des valeurs sont inférieures ou égales à $M_e$ et au moins 50\% des valeurs sont supérieures ou égales à $M_e$.
\end{definition}

Dans le cas d'une série à caractère discret, la médiane s'obtient en ordonnant les valeurs dans l'ordre croissant et en prenant la valeur centrale si $N$ est impair et la moyenne des valeurs centrales si $N$ est pair.

Dans le cas d'une série à caractère continu, la médiane peut s'obtenir de manière graphique en prenant la valeur correspondant à $0,5$ sur le polygone des fréquences cumulées croissantes.

\begin{propriete}
Soit une série statistiques quantitative comportant $N$ données : $S=\left\lbrace x_1,x_2,...,x_N\right\rbrace $ telles que $x_1\leqslant x_2 \leqslant ...\leqslant x_N$.
\begin{itemize}
\item Si $N$ est impair, la $\dfrac{N+1}{2}$\up{ème} donnée de la série est la médiane.
\item Si $N$ est pair, tout nombre compris entre le $\dfrac{N}{2}$\up{ème} élément de la série et le suivant est une médiane; dans le cadre scolaire la médiane sera la moyenne des deux données centrales de la série : \[M_e = \dfrac{\left( \dfrac{N}{2}\right)^{\text{ème}}+\left( \dfrac{N}{2}+1 \right)^{\text{ème}} }{2}\]
\end{itemize}
\end{propriete}

\begin{exemple}
Déterminer la médiane de la série 1 et la classe médiane de la série 2.
\end{exemple}


\rep{La médiane de la série 1 est 4 qui correspond à la moyenne de la soixantième et de la soixante et unième valeur.\\
La centième valeur de la série 2 appartient à l'intervalle $[14;15[$ qui est donc la classe médiane.}

\subsubsection*{Moyenne}
\begin{definition}
La moyenne d'une série statistique $(x_i,n_i)$ est le réel, noté $\bar{x}$ défini par :
\[
\bar{x}= \frac{n_1 x_1+n_2 x_2+... + n_px_p}{N}=\frac{\sum\limits_{i=1}^{p}n_i x_i}{N}
\]
Dans le cas d'une série à caractère quantitatif continu dont les valeurs sont regroupées en classes, $x_i$ désigne le centre de chaque classe.
\end{definition}


\begin{exemple}
Déterminer la moyenne des séries 1 et 2.
\end{exemple}

\rep{
$\bar{x}_1=\dfrac{11\times1+18\times2+20\times3+24\times4+16\times5+14\times6+11\times7+6\times8}{11+18+20+24+16+14+11+6}=4,1$\\
$\bar{x}_2=\dfrac{28\times12,5+46\times13,5+65\times14,5+32\times15,5+29\times16,5}{28+46+65+32+29}=14,44$}

\subsection{Paramètres de position non centrale}
\subsubsection*{Quartiles}
\begin{definition}
Le premier quartile $Q_{1}$ est la plus petite valeur du caractère telle qu'au moins $25\%$ des termes de la série aient une valeur qui lui soit inférieure ou égale.

Le troisième quartile $Q_{3}$ est la plus petite valeur du caractère telle qu'au moins $75\%$ des termes de la série aient une valeur qui lui soit inférieure ou égale.
\end{definition}

Dans le cas d'une série à caractère discret, les quartiles s'obtiennent en ordonnant les valeurs dans l'ordre croissant puis :
\begin{itemize}
\item Si $N$ est multiple de 4 alors $Q_1$ est la valeur de rang $\frac{N}{4}$ et $Q_3$ est la valeur de rang $\frac{3N}{4}$.
\item Si $N$ n'est pas multiple de 4 alors $Q_1$ est la valeur de rang immédiatement supérieur à $\frac{N}{4}$ et $Q_3$ est la valeur de rang immédiatement supérieur à $\frac{3N}{4}$.
\end{itemize}
\begin{exemple}
Déterminer les quartiles de la série 1
\end{exemple}
\rep{
Le nombre de données est multiple de 4 : 120. Le premier quartile est donc la 30\up{e} valeur et le troisième quartile la 90\up{e} valeur. On a ainsi : $Q_1=3$ et $Q_3=6$.}

Dans le cas d'une série à caractère continu, les quartiles peuvent s'obtenir à partir du polygone des fréquences cumulées croissantes où $Q_{1}$ est la valeur correspondant à la fréquence cumulée croissante égale $0,25$ et  $Q_{3}$ est la valeur correspondant à la fréquence cumulée croissante égale $0,75$.

\begin{exemple}
Représenter le polygone des fréquences cumulées croissantes de la série 2. Déterminer graphiquement la médiane et les quartiles.
\end{exemple}

\begin{tikzpicture}
\draw[gray,dotted] (0,0) grid[xstep=0.5,ystep=1] (18,10);
\draw[-stealth] (0,0)--(18,0) node{$x$};
\draw[-stealth] (0,0)--(0,10) node{$y$};
\foreach \x in {1,2,...,17} \draw(\x,0)node[below]{\x};
\foreach \y in {1,2,...,10} \draw(0,\y)node[left]{\y0};
\draw (0,0)--(13,1.4) node{$\bullet$} --(14,3.7)node{$\bullet$} --(15,6.95)node{$\bullet$}--(16,8.55)node{$\bullet$} --(17,10)node{$\bullet$};
\draw[very thick, red] (0,5)-|(14.4,0) node[above] {Médiane};
\draw[very thick, red] (0,2.5)-|(13.5,0) node[above left] {$Q_1$};
\draw[very thick, red] (0,7.5)-|(15.3,0) node[above right] {$Q_3$};
\end{tikzpicture}
\rep{
On obtient $Q_1\simeq13,5$, $M_e=14,4$ et $Q_3\simeq15,3$
}

\begin{exemple}
Retrouver ces résultats par le calcul.
\end{exemple}

\rep{Le premier quartile est dans la classe $[13;14[$. En posant $A(13;14)$ et $B(14;37)$, $Q_1$ est le point de $(AB)$ d'ordonnée 25.\\
Le coefficient directeur de $(AB)$ est égal à $\dfrac{37-14}{14-13}=23$ et l'ordonnée à l'origine se calcule en utilisant, par exemple, les coordonnées de $A$.\\
On obtient l'équation suivante : $(AB) : y=23x-285$\\
$Q_1$ étant le point de $(AB)$ d'ordonnée 25, il est solution de l'équation $25=23Q_1-285$. On obtient $Q_1=\dfrac{310}{23}\simeq13,5$\\
Avec des raisonnements analogues, on obtient $M_e=14,4$ et $Q_3\simeq15,3$}


\section{Paramètres de dispersion}
\subsection{Étendue}
\begin{definition}
L'étendue est la différence entre la plus grande valeur du caractère et la plus petite.
\end{definition}

Remarque : L'étendue est très sensible aux valeurs extrêmes.

\begin{exemple}
Déterminer l'étendue des séries 1 et 2
\end{exemple}

\rep{
L'étendue de la série 1 est $8-1=7$.\\
L'étendue de la série 2 est $17-12=5$.}

\subsection{Écart interquartile}
\begin{definition}
L'intervalle interquartile est l'intervalle $[Q_{1};Q_{3}]$.

L'écart interquartile est le nombre $Q_{3}-Q_{1}$. C'est la longueur de l'intervalle interquartile.
\end{definition}

Remarque : Contrairement à l'étendue, l'écart interquartile élimine les valeurs extrêmes, ce peut être un avantage. En revanche il ne prend en compte que $50\%$ de l'effectif, ce peut être un inconvénient.

\begin{exemple}
Déterminer l'intervalle interquartile et l'écart interquartile des séries 1 et 2.
\end{exemple}

\rep{Pour la série 1 : l'intervalle interquartile est $[3;6]$. L'écart interquartile est donc 3.\\
Pour la série 2 : l'intervalle interquartile est $[13,5;15,3]$. L'écart interquartile est donc 1,8.}

\subsection{Diagramme en boîte}
On construit un diagramme en boîte de la façon suivante :
\begin{itemize}
\item  les valeurs du caractère sont représentées sur un axe (vertical ou horizontal) ;
\item  on place sur cet axe, le minimum, le maximum, les quartiles et la médiane de la série ;
\item  on construit alors un rectangle parallèlement à l'axe, dont la longueur est l'interquartile et la largeur arbitraire.
\end{itemize}
\begin{tikzpicture}[scale=0.9]
\draw[very thick,-stealth](0,0)--(20,0);
\begin{scope}[xshift= 5cm, yshift=0.5cm]
\draw rectangle(2,1) node[above] {Médiane};
\draw rectangle(10,1);
\end{scope}
\draw (2,1) node{$\bullet$} node[above]{min} -- (5,1) node {$\bullet$} node[right]{$Q_1$};
\draw (15,1)node {$\bullet$} node[left]{$Q_3$} -- (19,1)node{$\bullet$} node[above]{max};
\end{tikzpicture}
Remarque : Ce diagramme permet non seulement de visualiser la dispersion d'une série mais aussi de comparer plusieurs séries entre elles.

\begin{exemple}
Construire le diagramme en boîte de la série 1
\end{exemple}

\rep{\begin{tikzpicture}[scale=0.9]
\tkzInit[xmin=0,xmax=9]
\tkzAxeX
\begin{scope}[xshift= 3cm, yshift=0.5cm]
\draw rectangle(1,1) node[above] {Médiane};
\draw rectangle(3,1);
\end{scope}
\draw (1,1) node{$\bullet$} node[above]{min} -- (3,1) node {$\bullet$} node[right]{$Q_1$};
\draw (6,1)node {$\bullet$} node[left]{$Q_3$} -- (8,1)node{$\bullet$} node[above]{max};
\end{tikzpicture}}

\subsection{Variance et écart-type}
Pour mesurer la dispersion d'une série, on peut s'intéresser à la moyenne des distances des valeurs à la moyenne. On utilise plutôt les carrés des distances qui facilitent les calculs.

\begin{definition}
On appelle variance d'une série quelconque à caractère quantitatif discret le nombre :
\[
V=\frac{1}{N}\sum_{i=1}^{p}n_{i}(x_{i}-\bar{x})^{2}=\sum_{i=1}^{p}f_{i}(x_{i}-\bar{x})^{2}
\]
On appelle écart-type de cette série le nombre $\sigma=\sqrt{V}$.
\end{definition}

Dans le cas d'une série à caractère quantitatif continu dont les valeurs sont regroupées en classes, $x_i$ désigne le centre de chaque classe.

\begin{propriete}
On peut calculer la variance de la façon suivante :
\[V=\frac{1}{N}\sum_{i=1}^{n}n_{i}x_{i}^{2}-\bar{x}^{2}\]
\end{propriete}


\begin{demonstration}
\begin{align*}
V&=\frac{1}{N}\sum_{i=1}^{p}n_{i}(x_{i}-\bar{x})^{2}
= \frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2}-2n_{i}x_{i}\bar{x}+n_{i}\bar{x}^{2}\\
&= \frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2} - 2\bar{x}\times\frac{1}{N}\sum_{i=1}^{n}n_{i}x_{i} + \bar{x}^{2}\times\frac{1}{N}\sum_{i=1}^{n}n_{i}
= \frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2}-2\bar{x}^2+\bar{x}^{2}\\
&= \frac{1}{N}\sum_{i=1}^{n}n_{i}x_{i}^{2}-\bar{x}^{2}
\end{align*}
\end{demonstration}

\begin{exemple}
Déterminer les variances et écart-types des séries 1 et 2.
\end{exemple}

\rep{Pour la série 1 : $V_1=\dfrac{571}{150}\simeq3,8$ et $\sigma_1=\sqrt{V_1}\simeq1,95$\\
Pour la série 2 : $V_2=1,5264$ et $\sigma_1=\sqrt{V_2}\simeq1,24$}

\begin{propriete}
La fonction $g~: t \longmapsto  \frac{1}{N}\sum_{i=1}^{p} n_{i}(x_{i}-t)^{2}$ admet un minimum atteint en $t=\bar{x}$ (la moyenne de la série) et ce minimum vaut $V$ (la variance de la série).
\end{propriete}

\begin{demonstration}
Pour tout $t\in\R$ :
\begin{multline*}
g(t)=\frac{1}{N}\sum_{i=1}^{p} n_{i}(x_{i}-t)^{2}
= \frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2}-2n_{i}x_{i}t+n_{i}t^{2}\\
= \left( \frac{1}{N}\sum_{i=1}^{p}n_{i} \right)t^{2} - 2\times\left(\frac{1}{N}\sum_{i=1}^{n}n_{i}x_{i} \right)t + \frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2}
= t^{2}-2\bar{x}t+\frac{1}{N}\sum_{i=1}^{p}n_{i}x_{i}^{2}
\end{multline*}
$g$ est un polynôme du second degré qui atteint son minimum en $\dfrac{2\bar{x}}{2}=\bar{x}$. Ce minimum est $g(\bar{x})=\frac{1}{N}\sum_{i=1}^{p} n_{i}(x_{i}-\bar{x})^{2}=V$

\end{demonstration}
\section{Représentation graphique}
Si les mesures centrales et les mesures de dispersion ont pour but de r\'esumer une s\'erie statistique en quelques nombres, les repr\'esentations graphiques, elles, visent \`a la visualiser.

\subsection{Diagramme à bâtons}

On considère la série :
%\vspace{-1em}
\begin{center}
\begin{tabular}{|*{22}{c|}}\hline
Valeurs $x_i$ & 0 & 1 & 2& 3 & 4& 5& 6\\ \hline
Effectifs $n_i$ & 3 & 5 & 6& 5 & 6& 7& 7\\ \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tikzpicture}
  \tkzInit[xmin=0,xmax=6,ymin=0,ymax=7]
    \tkzAxeX[unit]
        \tkzAxeY[unit]
        \draw [very thick, blue] (0,0)--(0,3);        
        \draw [very thick, blue] (1,0)--(1,5);
        \draw [very thick, blue] (2,0)--(2,6);
        \draw [very thick, blue] (3,0)--(3,5);
        \draw [very thick, blue] (4,0)--(4,6);
        \draw [very thick, blue] (5,0)--(5,7);
        \draw [very thick, blue] (6,0)--(6,7);
\end{tikzpicture}
\end{center}


\subsection{Diagrammes bas\'es sur la fr\'equence}

 Les s\'eries statistiques peuvent aussi \^etre repr\'esent\'ees en diagrammes circulaires, semi-circulaires, rectangulaires, etc.
L'aire de chaque modalit\'e devra \^etre proportionnelle \`a l'effectif de cette modalit\'e.
Les fr\'equences permettent d'obtenir assez facilement la part du diagramme qui devra \^etre consacr\'ee \`a chaque modalit\'e.

 Ainsi si on consid\`ere la s\'erie suivante :
\begin{center}
\begin{tabular}{|*{5}{c|}}\hline
Donn\'ees & Blonds 	& Bruns & Ch\^atains & Roux  \\ \hline
Effectifs $n_i$ & 25		& 57		&91		&23 \\ \hline
\end{tabular}
\end{center}            

 On obtient les diagrammes de la figure\\
\begin{center}
 \begin{tikzpicture}
\draw[fill=red] (0,0)--(0:2.5)arc(0:52.2:2.5)--cycle;
\draw[fill=green] (0,0)--(52.2:2.5)arc(52.2:154.8:2.5)--cycle;
\draw[fill=blue] (0,0)--(154.8:2.5)arc(154.8:196.2:2.5)--cycle;
\draw[fill=violet] (0,0)--(196.2:2.5)arc(196.2:360:2.5)--cycle;
 \end{tikzpicture}
\end{center}



\subsection{Diagramme en boite}

 On peut représenter graphiquement les valeurs extrêmes, les quartiles et la médiane par un \emph{diagramme en boite}, appelé aussi \emph{boite à moustaches}, conçu de la manière suivante :
\begin{itemize}
	\item au centre une boite allant du premier au troisième quartile, séparée en deux par la médiane ;
	\item de chaque côté une moustache allant du minimum au premier quartile pour l'une, et du troisième quartile au maximum pour l'autre.
\end{itemize}

\begin{tikzpicture}[scale=0.9]
\tkzInit[xmin=0,xmax=20]
\tkzAxeX
\begin{scope}[xshift= 5cm, yshift=0.5cm]
\draw rectangle(2,1) node[above] {Médiane};
\draw rectangle(10,1);
\end{scope}
\begin{scope}[xshift= 4cm, yshift=2.5cm]
\draw rectangle(6,1) node[above] {Médiane};
\draw rectangle(10,1);
\end{scope}
\draw (2,1) node{$\bullet$} node[above]{min} -- (5,1) node {$\bullet$} node[right]{$Q_1$};
\draw (15,1)node {$\bullet$} node[left]{$Q_3$} -- (19,1)node{$\bullet$} node[above]{max};
\draw (2,3) node{$\bullet$} node[above]{min} -- (4,3) node {$\bullet$} node[right]{$Q_1$};
\draw (14,3)node {$\bullet$} node[left]{$Q_3$} -- (18,3)node{$\bullet$} node[above]{max};
\end{tikzpicture}

Ce type de diagramme permet une interprétation visuelle et rapide de la dispersion des séries statistiques. Il
permet également d'apprécier des différences entre des séries (lorsqu'elles ont des ordres de grandeurs
comparables).

Remarque\\
\begin{itemize}
	\item La hauteur des boites est arbitraire (on les fait parfois proportionnelles à l'effectif total de la série).
	\item La boite contient les 50\% des données centrales.

\end{itemize}
\section{Résumé d'une série statistique}
On résume souvent une série statistique par une mesure de tendance centrale associée à une mesure de dispersion. les plus utilisées sont les suivantes :
\begin{itemize}
\item Le couple \textit{médiane ; écart interquartile}.

Il est insensible aux valeurs extrêmes et permet de comparer rapidement deux séries (par exemple grâce au diagramme en boîte) mais sa détermination n'est pas toujours pratique car il faut classer les données et il n'est pas possible d'obtenir la médiane d'un regroupement de séries.
\item Le couple \textit{moyenne ; écart-type}.

Il est sensible aux valeurs extrêmes mais se prête mieux aux calculs. On peut notamment obtenir la moyenne et l'écart-type d'un regroupement de séries connaissant la moyenne et l'écart-type des séries de départ.
\end{itemize}
\begin{exercice}
\end{exercice}
\end{document}