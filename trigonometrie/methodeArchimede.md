---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Calcul de $\pi$ avec la méthode d'Archimède.

+++ {"tags": ["M\u00e9thode", "approximation", "Archim\u00e8de", "pi"]}

## Description de la méthode d'Archimède
La méthode d'Archimède permet d'obtenir une approximation du nombre $\pi$. Pour cela on calcule les périmètres de polygones réguliers inscrits et circonscrits à un cercle de rayon $\dfrac{1}{2}$. Plus le nombre de côtés du polygone sera important, plus on se rapprochera du périmètre du cercle, à savoir $\pi$.
![archimede.png](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fe%2Fe7%2FDomenico-Fetti_Archimedes_1620.jpg%2F220px-Domenico-Fetti_Archimedes_1620.jpg&f=1&nofb=1)

+++

## Calcul du périmètre des polygones

On pose $a_n$ le périmètre d'un polygone régulier ayant $n$ côtés et inscrit dans le cercle de rayon $\dfrac{1}{2}$ et $b_n$ le périmètre d'un polygone régulier ayant $n$ côtés et circonscrit au cercle de rayon $\dfrac{1}{2}$.
On vérifie que :
$$
a_n = n \sin ( \frac{\pi}{n}) \text{ et }b_n = n \tan ( \frac{\pi}{n}).
$$

+++

- **Mathématiques débranchées**

 Tracer les polygones réguliers à 3 côtés, à 4 côtés, puis à 6 côtés.

+++

## Implémentation de la méthode 
On peut implémenter cette méthode utilisant les fonctions $\textrm{sinus}$ et $\textrm{tangente}$ (qui étaient inconnues d'Archimède).

Écrire une fonction qui calcule les termes de suites $a_n$ et $b_n$.

```{code-cell} ipython3
from math import sin,tan,pi

def archimede_simple(n):
    ...
```


```{code-cell} ipython3
:tags: [hide-input]
from math import sin,tan,pi

def archimede_simple(n):
    return n*sin(pi/n),n*tan(pi/n)

archimede_simple(5)
```


```{code-cell} ipython3
a,b = archimede_simple(5)
a - pi, b - pi
```

+++ {"tags": ["Simulation", "Animation"]}

## Visualisation de la convergence
Cette animation montre sur la figure de gauche les polygones inscrits et circonscrits et donne sur la figure de droite le périmètre de chacun. On peut ainsi visualiser la convergence vers $\pi$.

```{code-cell} ipython3
:tags: [Simulation, Animation, remove-input]

import matplotlib.pyplot as plt
from matplotlib.patches import Circle,RegularPolygon
from IPython.display import HTML
import matplotlib.animation
from math import cos

# nombre de cotés maximum - 3
N=15

fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(12, 6))
l= .7
ax1.set_xlim(( -l, l))
ax1.set_ylim((-l, l))
ax2.set_xlim(( 4, N+3))
ax2.set_ylim((2.75, 4.25))
approximationsInf = []
approximationsSup = []

cercle = Circle((0, 0), .5, facecolor='none',
                edgecolor=(0, 0, 0), linewidth=2, alpha=0.75)
courbeInf, = ax2.plot([],[],'-o',color="#1e7fcb")
courbeSup, = ax2.plot([],[],'-o',color='orange')

def init():
    return []

def animate(i):
    inf,sup = archimede_simple(i+4)
    approximationsInf.append(inf)
    approximationsSup.append(sup)
    ax1.clear()
    ax1.set_xlim(( -l, l))
    ax1.set_ylim((-l, l))
    ax1.add_patch(cercle)
    long = 0.5/cos(pi/(i+4))
    PI = RegularPolygon(numVertices = 4+i,xy=(0, 0), radius=.5, orientation=0.79,edgecolor="#1e7fcb", facecolor='none',
                linewidth=2, alpha=0.5)
    PS = RegularPolygon((0, 0), 4+i, radius=long, orientation=.79, facecolor='none',
                edgecolor='orange', linewidth=2, alpha=0.5)
    ax1.add_patch(PI)
    ax1.add_patch(PS)
    ax1.set_title('{} côtés'.format(i+4),color="#1e7fcb",fontsize=14)
    courbeInf.set_data(range(4,i+5),approximationsInf)
    courbeSup.set_data(range(4,i+5),approximationsSup)
    return PI

ax2.plot([4,3+N],[pi,pi],'--',color='green')
ax2.legend(['Polygones intérieurs','Polygones extérieurs','$\pi$'])

plt.close ()
ani = matplotlib.animation.FuncAnimation(fig, animate,init_func=init,frames=N,blit=False,interval=500)
# l'un ou l'autre
HTML(ani.to_jshtml())
#HTML(ani.to_html5_video())
```

## Accélération de la convergence

Pour tout entier naturel $n$, on pose $\alpha_n=a_ {2^n}$ et $\beta_n=b_{2^n} $.


$$
\lim _{n\rightarrow +\infty} \alpha_n = \lim _{n\rightarrow +\infty} \beta_n = \pi.
$$

On peut démontrer que 
$$
b_{2n} = \frac{2a_nb_n}{a_n+b_n} \text{ et }a_{2n}=\sqrt{a_nb_{2n}}.
$$

En déduire que $\alpha_n=\sqrt{\alpha_{n-1}\beta_n}$ et 
$\beta_n=\dfrac{2\alpha_{n-1}\beta_{n-1}}{\alpha_{n-1}+\beta_{n-1}}$.

+++

## Programmation des suites $(a_{2^n})$ et $(b_{2^n})$
Dans cette partie, on utilise les relations précédentes pour calculer les termes des suites $(a_{2^n})$ et $(b_{2^n})$.

Écrire le programme `archimede` qui calcule les valeurs en utilisant les formules précédentes.

```{code-cell} ipython3
from math import sqrt

def archimede(n):
    ...
```

```{code-cell} ipython3
:tags: [suites, hide-input]

from math import sqrt

def archimede(n):
    a = 2*sqrt(2)
    b = 4
    for i in range(n):
        b = (2*a*b)/(a+b)
        a = sqrt(b*a)
    return a,b
print(archimede(10))
```


+++ {"tags": ["Convergence"]}

## Convergence de la deuxième méthode

+++

Le programme suivant permet de visualiser la convergence vers $\pi$ par la méthode 2.

```{code-cell} ipython3
:tags: [remove-input]
# nombre d'itérations (on initialise au cas du carré)
N = 3
approximationsInf = []
approximationsSup = []
abscisse = []

for i in range(N):
    abscisse.append(2**(i+2))
    inf,sup = archimede(i)
    approximationsInf.append(inf)
    approximationsSup.append(sup)


plt.plot(abscisse,approximationsInf,'-o')
plt.plot(abscisse,approximationsSup ,'-o')
plt.plot([4,2**(N+1)],[pi,pi])
plt.legend(['Polygones intérieurs','Polygones extérieurs','$\pi$'])
plt.show()
```

+++ {"tags": ["Simulation", "Animation"]}

## Visualisation de la convergence de la deuxième méthode
Cette animation montre la convergence vers $\pi$ de la deuxième méthode. On peut constater que la convergence semble très rapide. La section suivante permettra de comparer les deux méthodes.

```{code-cell} ipython3
:tags: [remove-input]

# nombre d'itérations (on initialise au cas du carré)
N = 4

fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(12, 6))
l= .7
ax1.set_xlim(( -l, l))
ax1.set_ylim((-l, l))
ax2.set_xlim(( 4, 2**(N+1)))
ax2.set_ylim((3.05, 3.35))
approximationsInf = []
approximationsSup = []

cercle = Circle((0, 0), .5, facecolor='none',
                edgecolor=(0, 0, 0), linewidth=2, alpha=0.75)
courbeInf, = ax2.plot([],[],'-o',color="#1e7fcb")
courbeSup, = ax2.plot([],[],'-o',color='orange')
abscisse = []

def init():
    return []

def animate(i):
    abscisse.append(2**(i+2))
    inf,sup = archimede(i)
    approximationsInf.append(inf)
    approximationsSup.append(sup)
    ax1.clear()
    ax1.set_xlim(( -l, l))
    ax1.set_ylim((-l, l))
    ax1.add_patch(cercle)
    long = 0.5/cos(pi/(4*2**i))
    PI = RegularPolygon(numVertices = 4*2**i,xy=(0, 0), radius=.5, orientation=0.79,edgecolor="#1e7fcb", facecolor='none',
                linewidth=2, alpha=0.5)
    PS = RegularPolygon((0, 0), 4*2**i, radius=long, orientation=.79, facecolor='none',
                edgecolor='orange', linewidth=2, alpha=0.5)
    ax1.add_patch(PI)
    ax1.add_patch(PS)
    ax1.set_title('{} côtés'.format(4*2**i),color="#1e7fcb",fontsize=14)
    courbeInf.set_data(abscisse,approximationsInf)
    courbeSup.set_data(abscisse,approximationsSup)
    return PI,

ax2.plot([4,2**(N+1)],[pi,pi],'--',color='green')
ax2.legend(['Polygones intérieurs','Polygones extérieurs','$\pi$'])

plt.close ()
ani = matplotlib.animation.FuncAnimation(fig, animate,init_func=init,  frames=N,blit=False,interval=750)
# l'un ou l'autre
HTML(ani.to_jshtml())
#HTML(ani.to_html5_video())
```

## Précision

On cherche une valeur approchée de $\pi$ à $10^{-p}$ près. Pour cela, on remarque que 
$$
\forall n>0,\ a_n \leq \pi \leq b_n
$$
Ainsi, il suffit d'avoir $b_n-a_n \leq 10^{-p}$ pour obtenir l'approximation cherchée avec $a_n$ ou $b_n$ au choix.

+++

Le programme `archimedeSimplePrecision` prend en paramètre un entier naturel `p` et renvoie `a`, `b` et `n` tels que
$$b_n-a_n \leq 10^{-p}$$
avec $p$=`p`, $n$=`n`, $a_n$=`a` et $b_n$=`b` et en utilisant la fonction `archimedeSimple`.

```{code-cell} ipython3
def archimede_simple_precision(p):
    n = 4
    a,b = archimede_simple(4)
    while b-a>10**-p:
        n = n+1
        a,b = archimede_simple(n)
    return a,b,n
archimede_simple_precision(10)
```

* **Expliquer le programme** 
  1. A-t-on besoin d'une valeur absolue dans la condition de la ligne 4 ?
  1. À quelle condition sort-on de la boucle ?
  3. Que représentent `p` et `n`?


+++

Le programme `archimede_simple_precision` prend en paramètre un entier naturel `p`et renvoie `a`, `b` et `n`tels que
$$b_n-a_n \leq 10^{-p}$$
avec $p$=`p`, $n$=`n`, $a_n$=`a` et $b_n$=`b` et en utilisant la seconde méthode d'Archimède.

```{code-cell} ipython3
def archimede_precision(p):
    a = 2*sqrt(2)
    b = 4 
    n = 4 #
    while b-a>10**(-p):
        b = (2*a*b)/(a+b)
        a = sqrt(b*a)
        n = n+1 #
    return a,b,n
archimede_precision(10)
```

Réécrire la fonction `archimede_precision` en utilisant la fonction `archimede`.


```{code-cell} ipython3
def archimede_precision(p):
    ...
    return a,b,n
```

```{code-cell} ipython3
:tags: [hide-input]
def archimede_precision(p):
    a,b = archimede(4)
    n = 4 #
    while b-a>10**(-p):
        n = n+1 #
        a,b = archimede(n)
    return a,b,n
archimede_precision(10)
```


+++

## Comparaison des méthodes

```{code-cell} ipython3
:tags: [hide-cell]
import matplotlib.pyplot as plt
```

Le programme suivant nous permet de visualiser la vitesse de convergence de chaque méthode en utilisant la précision. En abscisse, il y a un entier $p$ et en ordonnée $n$ tel que :
$$b_n-a_n \leq 10^{-p}.$$

```{code-cell} ipython3
:tags: [hide-input]
n = 10
abscisse = [i for i in range(1,n+1)]
methode1 = [archimede_simple_precision(i)[2] for i in abscisse] 
methode2 = [archimede_precision(i)[2] for i in abscisse] 

plt.plot(abscisse,methode1,'o-')
plt.plot(abscisse,methode2,'o-')
plt.legend(['Méthode 1','Méthode 2'])
plt.show()
```


Remarque


La méthode 2 est donc beaucoup plus avantageuse car elle converge nettement plus rapidement et parce qu'elle n'utilise pas les fonctions trigonométriques.


% https://www.numworks.com/fr/professeurs/activites-pedagogiques/premiere/archimede/
% https://irem.univ-reunion.fr/spip.php?article445