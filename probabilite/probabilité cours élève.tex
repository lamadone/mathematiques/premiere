\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Probabilité}
\author{1S}
\date{}

\begin{document}

\maketitle\\
\section{Vocabulaire}
\begin{definition} Une \textcolor{red}{ expérience aléatoire} est un processus dont le résultat est incertain.

On appelle \textcolor{red}{univers} d'une expérience aléatoire l'ensemble $\Omega$ des \textcolor{red}{issues} possibles de l'expérience (ou \textcolor{red}{événements élémentaires}).

Dans ce chapitre, on supposera que l'univers est un ensemble fini.

Définir la \textcolor{red}{loi de probabilité} d'une expérience aléatoire dont l'univers est fini, c'est associer à chaque issue possible un nombre entre $0$ et $1$ (sa \textcolor{red}{probabilité}) qui représente les chances ou les risques que l'expérience aboutisse à ce résultat. 

La somme des probabilités de chacune des issues possibles doit valoir $1$.
\end{definition}

\begin{definition} 
La loi de probabilité d'une expérience aléatoire est dite \textit{équirépartie} si chaque événement élémentaire a la même probabilité. \\
Si l'univers $\Omega$ compte $n$ issues possibles, la probabilité de chacune des issues est donc $\frac 1n$.
\end{definition}

\begin{exemple} On considère l'expérience aléatoire consistant au lancer d'un dé cubique numéroté de 1 à 6 équilibré.
\begin{enumerate}
\item Quelle indication signifie que la loi de probabilité est équirépartie ? \\
\ligne[2]
\item Lister les issues qui composent l'univers de l'expérience :\\
\ligne[2]
\item Décrire la loi de probabilité de cette expérience :
$\begin{array}{|l|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|} \hline \text{Issue} & & & & & & \\ \hline \text{Probabilité} & & & & & & \\ \hline\end{array}$
\end{enumerate} 
\end{exemple}

\begin{definition} 
Étant donnée une expérience aléatoire, un événement $A$ est une partie de l'univers $\Omega$ : il est donc composé d'un certain nombre d'issues possibles de l'expérience.

La probabilité d'un événement $A$ est le nombre noté $p(A)$ qui est la somme des probabilités de chacunes des issues qui composent l’événement $A$. Ce nombre représente la chance ou le risque que l’événement se produise.
\end{definition}

\begin{exemple} 
 On reprend l'exemple  du dé.
Soit $A$ l’événement \og le résultat est strictement plus grand que 4\fg.\\
 On note $A=\{5,6\}$ et $p(A)=$ \dotfill
 
 Soit $B$ l'événement \og le résultat est pair\fg. $B=$\dotfill

$p(B)=$\dotfill
\end{exemple}

\begin{definition} 
\begin{itemize}
\item  L’événement \textcolor{red}{certain} $\Omega$ est composé de toutes les issues possibles :  \[p(\Omega)=1\]
Il est certain que cet événement se réalise.\\
\item L’événement \textcolor{red}{impossible} $\varnothing$ ne contient aucune des issues possibles : \[p(\varnothing)=0\]
Il est certain que cet événement ne se réalise pas.\\
\item L’événement \textcolor{red}{contraire} de l’événement $A$ est l’événement $\overline{A} $ composé des toutes les issues de l'univers qui ne sont pas dans $A$. \[p(\overline{A} )=1-p(A)\]
\item L’événement \textcolor{red}{élémentaire} de l'univers $\Omega$ est un événement contant une seule issue.\\
\item L’événement \textcolor{red}{$A\cup B$} est la réunion des ensembles $A$ et  $B$. C'est l'ensemble des éléments contenu dans les ensembles $A$ ou $B$.\\
\item L’événement \textcolor{red}{$A \cap B$} est l'intersection des ensembles $A$ et $B$. C'est l'ensemble des éléments contenu dans les ensembles $A$ et $B$.\\
\item \'Evévnements  \textcolor{red}{incompatibles} ce sont des événements qui n'ont pas d'éléments en commun. \[A \cap B =\varnothing\]
\end{itemize}
\end{definition}

\begin{propriete} [Probabilité totale]
Une \emph{partition} de l'univers $\Omega$ est un ensemble d’événements deux à deux incompatibles $A_1,...,A_k$ tels que $A_1\cup ...\cup A_k=\Omega$. 
On a alors \[p(A_1)+...+p(A_k)=1\]
\end{propriete}

\begin{propriete} 
\[p(A\cup B)=p(A)+p(B)-p(A\cap B)\]
\end{propriete}

\begin{exemple} 
Sur $1\,000$ ordinateurs vendus, un marchand observe que $80$ ont exigé une réparation dans la deuxième année qui a suivi l'achat (événement noté $R_2$) et $125$ lors de la troisième année qui a suivi l'achat ($R_3$), dont $20$ avaient déjà été réparés lors de la deuxième année. \\
Compléter le tableau suivant :\\
\begin{Large}
\[\begin{array}{|c|c|c|c|} \hline  & R_2 & \overline{R_2} \vphantom{\dfrac ab} &\text{Total} \\
\hline \vphantom{\dfrac ab}  R_3 &\phantom{\text{total}} &\phantom{\text{Total}} & \\ 
\hline  \vphantom{\dfrac ab}\overline{R_3} & & & \\ 
\hline \vphantom{\dfrac ab}\text{Total} & & &1\,000 \\ \hline 
\end{array}\]
\end{Large}
Sachant que l'ordinateur a été réparé la seconde année, quelle est la probabilité qu'il le soit aussi la troisième ? (décrire l'univers !) Exprimer ce résultat à l'aide de $p(R_2)$ et $p(R_2\cap R_3)$.\\
\ligne[10]
\end{exemple}

\section{Variable aléatoire}
\begin{definition} Définir une \textcolor{red}{variable aléatoire} $X$, c'est associer un nombre réel à chacune des issues de $\Omega$.\\
L’événement \og $X=k$\fg{} est l'ensemble des issues pour lesquelles $X$ vaut $k$.\\
 La \textcolor{red}{loi de probabilité} d'une variable aléatoire $X$ associe à chaque valeur possible $k$ de la variable $X$ la probabilité de l’événement \og$X=k$\fg{}.  \\
L'\textcolor{red}{espérance} de cette variable aléatoire est le nombre \[E(X)= \sum\limits_{k = 1}^{k= n} {k\times p(X=k)}\]
La \textcolor{red}{variance} de cette loi est le nombre \[Var(X)=\sum\limits_{k= 1}^{k = n} {(k- E(X))^2\times p(X=k)}\]
Ou bien $Var(X)=
    \sum\limits_{k = 1}^{k = n} {{p(X=k)}k^2 - {{\left[ {E\left( X
              \right)} \right]}^2}} $ \\
 L'\textcolor{red}{écart-type} est le nombre $\sigma=\sqrt {Var(X)}$.
\end{definition}
Méthode :\\
Pour d\'{e}terminer la loi de probabilit\'{e} d'une variable al\'{e}atoire $X$ :\\
  \begin{itemize}
 \item on d\'{e}termine les valeurs $k$ que peut   prendre $X$ ;\\
  \item on calcule les probabilit\'{e}s $P\left( {X = k}  \right)$ ;\\
  \item on r\'{e}sume les r\'{e}sultats dans un tableau.
  \end{itemize}

\begin{exemple}   Une urne contient cinq jetons indiscernables au toucher  num\'{e}rot\'{e}s de 1 \`{a} 5.\\
  Un joueur participe \`{a} la loterie en payant 2~\euro, ce qui lui  donne le droit de pr\'{e}lever au hasard un jeton dans l'urne.
  \begin{itemize}
  \item Si le num\'{e}ro est pair, il gagne en euros le  double de la valeur indiqu\'{e}e par le jeton.
  \item Si le num\'{e}ro est impair, il perd sa mise.
  \end{itemize}
  Soit $X$ la variable al\'{e}atoire \'{e}gale au «gain  alg\'{e}brique». \\
  D\'{e}terminer la loi de probabilit\'{e} de $X$.\\
  \ligne[26]
\end{exemple}
\begin{propriete}
Soit $Y$ une variable aléatoire telle que $Y=aX+b$
\[
E\left( {Y} \right) = aE(X) + b\ \text{\quad et\quad }  Var\left( {Y} \right) =
{a^2}Var \left( X \right).
\]
\end{propriete}
\ligne[18]
\begin{exemple} 
On donne $E(X)=3$ et $Var(X)=16$\\
\begin{enumerate}
\item Calculer $E(-2X+5)$ : \\
\ligne[2]
\item Calculer $Var(-2X+5)$ : \\
\ligne[2]
\end{enumerate} 

\end{exemple}

\end{document}



